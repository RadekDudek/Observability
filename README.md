# One Observability

## Lab overwiev

This lab introduces you to

## Task 1: Setup

1. Select region to **N.Virginia (us-east-1)** on top right corner.
2. In the AWS Management Console on the Services menu, navigate to **CloudShell**.
3. Copy and past the following commands into the terminal:
```
curl -O https://raw.githubusercontent.com/aws-samples/one-observability-demo/main/cloud9-cfn.yaml

aws cloudformation create-stack --stack-name C9-Observability-Workshop --template-body file://cloud9-cfn.yaml --capabilities CAPABILITY_NAMED_IAM

aws cloudformation wait stack-create-complete --stack-name C9-Observability-Workshop

echo -e "Cloud9 Instance is Ready!!\n\n"
```
4. Select **Paste** when prompted by CloudShell Safe Paste to continue. It will take a few minutes
5. You shloud see the message "Cloud9 Instance is Ready!!" will output to the terminal. This indicates the successful creation of your Cloud9 environment.

## Task 2: Deploy Application

1. In the AWS Management Console on the Services menu, click **Cloud9**.
2. Click **Open** on the **observabilityworkshop** Cloud9 instance.
3. Click the **gear icon** in the top right to open the **Preferences** tab.
4. Select **AWS SETTINGS** from the left navigation menu.
5. Toggle off the **AWS managed temporary credentials** setting.
6. Close the **Preferences** tab.
7. Navigate to the terminal at the bottom of the screen. (If you do not see a terminal, click **Window** from the top menu, then **New Terminal**)
8. Remove existing temporary credentials. Execute the following command in the terminal:
```
rm -vf ${HOME}/.aws/credentials
```
9. Install tools and clone the repository. Execute the following command in the terminal:
```
curl -sSL https://raw.githubusercontent.com/aws-samples/one-observability-demo/main/PetAdoptions/envsetup.sh | bash -s stable
```
10. Configure AWS CLI with the current AWS Region as default. Execute the following commands in the terminal:
```
export ACCOUNT_ID=$(aws sts get-caller-identity --output text --query Account)
export AWS_REGION=$(curl -s 169.254.169.254/latest/dynamic/instance-identity/document | jq -r '.region')
echo "export ACCOUNT_ID=${ACCOUNT_ID}" | tee -a ~/.bash_profile
echo "export AWS_REGION=${AWS_REGION}" | tee -a ~/.bash_profile
aws configure set default.region ${AWS_REGION}
aws configure get default.region
```
11. Validate environment settings. Execute the following commands in the terminal:
```
test -n "$AWS_REGION" && echo AWS_REGION is "$AWS_REGION" || echo AWS_REGION is not set

aws sts get-caller-identity --query Arn | grep observabilityworkshop-admin -q && echo "You're good. IAM role IS valid." || echo "IAM role NOT valid. DO NOT PROCEED."
```
12. Once cloned successfully, execute the following command in the terminal:
```
cd workshopfiles/one-observability-demo/PetAdoptions/cdk/pet_stack
```
13. Execute the following command in the terminal:
```
npm install
```
14. Installs all npm packages. Execute the following command in the terminal:
```
cdk bootstrap
```
15. This may take some time to complete.
15. **(optional)** Add permissions to access EKS Console. We suggest that you put in the Role ARN that you use to log into your AWS Console that also has access to the Amazon EKS service. Execute the following command in the terminal and replace **<Enter your Role ARN**:
```
CONSOLE_ROLE_ARN=<Enter your Role ARN>
```
16. Deploy the stack. Execute the following commands in the terminal:
```
EKS_ADMIN_ARN=$(../../getrole.sh)

echo -e "\nRole \"${EKS_ADMIN_ARN}\" will be part of system\:masters group\n" 

if [ -z $CONSOLE_ROLE_ARN ]; then echo -e "\nEKS Console access will be restricted\n"; else echo -e "\nRole \"${CONSOLE_ROLE_ARN}\" will have access to EKS Console\n"; fi

cdk deploy --context admin_role=$EKS_ADMIN_ARN Services --context dashboard_role_arn=$CONSOLE_ROLE_ARN --require-approval never

cdk deploy Applications --require-approval never

```
17. It will take a few minutes for the stack to be deployed
18. Update kubeconfig. Execute the following commands in the terminal:
```
aws eks update-kubeconfig --name PetSite --region $AWS_REGION            
kubectl get nodes                                
```

## Task 3: Navigating the Interface

1. In the AWS Management Console on the Services menu, click **CloudWatch**.
2. In the left navigation menu under **Logs**, click on **Logs Insights**.
3. Select the log group called **/ecs/PetListAdoptions** from the **Select log group(s)** drop down. ou will see that a sample query is automatically placed in the query field.
4. Click **Run query**.

Sample queries

5. In the right menu, click **Fields**. You will see a list of fields that were discovered by CloudWatch automatically. This allows you to pick and include the fields that you want to use in the query using intellisense.
6. In the right menu, click **Queries**. You will see a list of sample queries sorted into categories such as service or frequency of use.
7. Select a query and click **Apply** on the query you are interested in. The query will be loaded for you, and you can choose to **Run query**.
Copy and paste the following query into the query editor.

Saved queries

8. Copy and paste the following query into the query editor.
```
fields @timestamp, pettype
| filter ispresent(pettype)
| stats count() by pettype
```
9. Click on **Run query** to see that it returns results.
10. Click the **Save** button underneath the query editor.
11. Enter **Sample1** for **Query name**.
12. Select **Save**.
13. In the right navigation menu, click **Queries**. You should now have a query called **Sample1** listed under **Saved queries**.

Query history

14. Click the **History** button underneath the query editor. Here you can see a history of all the queries you have executed.

## Task 2: Querying

1. In the AWS Management Console on the Services menu, click **CloudWatch.**
2. In the left navigation menu under **Logs**, click on **Logs Insights**.
3. From the **Select log group(s)** drop down, select the **/ecs/PetListAdoptions** log group. You will see that a sample query is automatically placed in the query field.
4. Click on **Run query** and view the results. At the top you can see a histogram showing the distribution of log events over time where they match your query.
5. Underneath you can see the events that match your query. You can click on the arrow on the left of each line to expand the event. In this case, because the event is in JSON we see it displayed as a list of the field names, with their corresponding value alongside.
6. Now you use the filter command to specify which log events we want to work with. Copy the query below and paste it into the query editor.
```
fields @timestamp, @message
| filter @message like /brown/
| sort @timestamp desc
| limit 20
```
7. This query applies a filter on the messages and fetches only the records that contain the string brown in the log. It also sorts the events which are found is descending time order, and returns only 20 of these.
8. Click on **Run query**.
9. We can use the stats command to aggregate data. Delete the query that is already in the query editor and copy and paste in the following query:
```
fields @timestamp, @message
| stats count(@message) as number_of_events by bin(5m)
| limit 20
```
10. Click on **Run query**.
11. This query returns a result that is the count of the number of messages captured in 5 minute buckets/bins.
12. You can also visualize the results by clicking on the **Visualization tab** in the results area as shown below. Notice that you can also add the visualization to a CloudWatch Dashboard, export to csv and so on.

## Task 3: Displaying results

1. In the AWS Management Console on the Services menu, click **CloudWatch**.
2. In the left navigation menu under **Logs**, click on **Logs Insights**.
3. From the **Select log group(s)** drop down, select the **/ecs/PetListAdoptions** log group.

Create a table display

4. Use the following query and **Run query** to see the results.
```
fields @timestamp, petcolor, pettype
| filter ispresent(petcolor) AND ispresent(pettype)
| stats count() by pettype
```
5. Choose to **Add to dashboard**
6. Choose to **Create new**.
7. Give the new dashboard a name of **display-options** and **Create**.
8. You can modify the title here (under **Customize widget title**), or modify it once it is on the dashboard. I have called this widget **table display**.
9. Choose **Add to dashboard**.
10. Resize your widget as desired, and **Save dashboard**.

Create a pie chart display

11. From your CloudWatch dashboard click on the **3 vertical dots** on the top right of the time chart widget previously created, and choose to **Duplicate**.
12. On your new widget, click on the 3 vertical dots again, and choose to **Edit**. You will be returned to the Log Insights view to edit your query.
13. Choose to **Run query**.
14. Choose the **Visualization** tab and choose a **Pie** chart display.
15. Choose **Save changes**.
16. Hover over the title and click on the edit (**pencil**) icon. Change the title to **pie chart display**.
17. Resize, reposition and rename your widget as desired. Remember to **Save dashboard**.

Create a bar chart display

18. Duplicate one of your widgets as before, then choose to **Edit** and **Run query**.
19. Choose the **Visualization** tab and choose a **Bar** chart display.
20. Choose **Save changes**.
21. Hover over the title and click on the edit (**pencil**) icon. Change the title to **bar chart display**.
22. Resize, reposition and rename your widget as desired. Remember to **Save dashboard**.

Create a time chart display

23. Duplicate one of your widgets as before, then choose to **Edit**.
24. Modify the stats command in your query to include a time aggregation as shown below, and **Run query**.
```
fields @timestamp, petcolor, pettype
| filter ispresent(petcolor) AND ispresent(pettype)
| stats count() by bin(5m)
```
25. Choose the **Visualization** tab and choose a **Line** chart display.
26. Choose **Save changes**.
27. Hover over the title and click on the edit (**pencil**) icon. Change the title to **time chart display**.
28. Resize, reposition and rename your widget as desired. Remember to **Save dashboard**.

Create a stacked area display

29. Duplicate one of your widgets as before, then choose to **Edit** and **Run query**.
30. Choose the **Visualization** tab and choose a **Stacked area** chart display.
31. Choose **Save changes**.
32. Hover over the title and click on the edit (**pencil**) icon. Change the title to **stacked area display**.
33. Resize, reposition and rename your widget as desired. Remember to **Save dashboard**.

## Task 5: Live Tail

You can access Live tail feature directly from the AWS CloudWatch console menu on the left, under the Logs section. From here you can select the log group and, optionally, the specific log stream to include in your tail session.

1. Click on **Log groups** under the Logs section on the left navigation menu
2. Select the checkbox for the Log group **/aws/containerinsights/PetSite/application**, the **Start Tailing** button becomes available
3. Click on **Start Tailing** button
4. When the Live Tail console starts, and you click on the **Filter** icon, you will be presented with the Filter configuration panel expanded showing the following available filtering options:
* Select a log group - this is pre-populated if the first method was used. To start tailing the live events, at least one log group must be selected. All other input fields and primary buttons will be enabled only after the log group selection is made. For this lab, ensure the log group named **/aws/containerinsights/PetSite/application is selected.**

* Select log streams [optional] - this option is only available when a single log group is selected. It is disabled when more than one log group is selected. If no selection is made, all log streams in the selected log group will be included. There are two ways to find a log stream:

    * By Name - click on the dropdown to see a list of the log streams and select individually, or, type a name to search and select.
    * By Prefix - Log Streams can be selected based on a common prefix.
For this lab lets leave this filter empty to all log streams are included.

* Add filter patterns [optional] - this option is case sensitive and allows to filter based on the content of the messages in the selected log groups and log streams. For this lab, use this value to retrieve only the logs from the petsite container:
```
{ $.kubernetes.container_name = "petsite" }
```

5. You can enter up to 5 terms to be highlighted in the tail session. Terms are not case sensitive and you'll see it in the tail window session. Type each one of the following words followed by Enter (don't separate with spaces): **dns udp recorder log failed**
6. Once the filter is applied, you will notice the color bars on the left side of the each event line, identifying which keywords were found.
7. Expand an event that has a color code next to it, and it'll show the exact place of the event that contains the keyword.
8. Use the magnifier icon located next to the event to open a new window that allows you to review it and copy the content to your clipboard. You can also access the specific source of this message from here in the **View trailing events** link.

## Task 6: Data Protection

Setup

1. Open the **CloudWatch** console 
2. In the navigation pane, choose **Logs**, **Log groups**.
3. Choose the name of the log group. **log-group:/ecs/PayForAdoption:***
4. Choose **Actions**, **Create data protection policy**.
5. For **Data identifiers**, select the types of data that you want to audit and mask in this log group. You can type in **CreditCardNumber** in selection box to find the identifiers.
6. Create a new **log group**.
7. For details about which types of data that you can protect, see Types of data that you can protect.
8. Choose **Activate data protection**.

View and Query

1. In the **CloudWatch** Console, select the Log Group you created in previous step.
2. You will notice all the logs which contains "**CreditCardNumber**" as identifier are captured in this LogGroup.
3. Alternatively, you can also sort LogGroup based on Sensitive Data Column. This will show you how many event of sensitive data were identified.
4. You can query the LogGroup with Logs Insights to view specific logs with CreditCardNumbers
5. Go to **LogInsights** on the left
6. Select LogGroup - **/ecs/PayForAdoption**
7. Copy and paste below code in query field
```
fields @timestamp, @message
| filter customer.CreditCard like //
| sort @timestamp desc
| limit 20
```
8. Click **Run Query**
9. **Expand** the first log, you will notice, the logs with "masked" CreditCard Number
10. For investigative purposes, you can also unmask the redacted fields. You can unmask the message by running following query.
```
fields @timestamp, @message, unmask(@message)
| filter customer.CreditCard like //
| sort @timestamp desc
```
11. Expand one of the logs, and scroll to the right. You will notice **CreditCard Number**

Metrics Notifications

1. Go to **All Metrics** on the left Panel
2. Click on **Logs -> DataProtectionOperation**
3. Select **Audit** for **LogGroup = /ecs/PayForAdoption**
4. Change the title of the Metrics on top left by clicking **edit** button
5. Click on **Actions** on the top and add to dashboard
6. Create a new dashboard and give it a name
7. Go to **Graphed Metrics** tab, and for above created graph, select **bell icon** under actions
8. On the next page, select **metrics and conditions**, For eg: Set threshold to greater than or equal to 1, select next
9. Create a new topic or select from existing and give it a random name, "observability-sns", enter your email id, then click **create topic** and click next
10. Give the newly created alarm a **name** and add some description
11. Select **next** and **create alarm** after reviewing the details
12. You will notice in Alarms homepage that, the state has changed to "in alarm" now.
13. Continue to the Petsite, and complete a transaction (adopt a new pet)
14. In about couple of minutes, you will see a notification email on detection of PII

## Task 7: Explore X-Ray

1. In the AWS Management Console on the Services menu, navigate to **CloudWatch** and select **Service map** under **X-Ray traces**.
3. Select a duration from the time drop down at the top right.
4. Click on a **any node**.
5. Click on the **arrow** between two nodes.
6. Click on **View traces** from the edge details section. You should see a screen similar to the one below.
7. Select a trace from the trace list table by clicking on a trace ID. If the trace contains an error, the Exception tab will show the error message information.
8. Return to the **Service map**  by navigating to the **Service map** section in the **CloudWatch Service** console.
9. Select the **payforadoption node**, then click **View traces**.
10. Refine the query by URL that ends with **/Payment/MakePayment** and select any one of the traces.
11. As shown on the screenshot below, if you click on a node on the trace, the associated segment will be selected.
12. Click on the segment **payforadoption**.
13. Click on the **Annotations** tab. You will be able to see the custom Annotations that were instrumented through the X-Ray SDK. You can see **PetId** and **PetType** being annotated in the trace.
14. Switch to the **Metadata** tab to see the metadata information that was added to the trace.
15. Close the current window and open the database subsegment called **adoptions**. For this subsgement, you will see a new tab called SQL with the prepared SQL query and various informations about the target database.
16. Select a **DynamoDB** subsegment.
17. Go to the **Resources** tab and check the details such as the table name, the operation that was performed on the table, and the region.

## Task 8: X-Ray Analytics

Before this part of lab please change for old version of the page

1. In the AWS Management Console on the Services menu, navigate **to X-Ray** and change for old version of the page.
2. Click on **Analytics** from the left navigation menu.
3. Select the area of the graph where you see higher latency.
4. Click on one of the urls in the list.
5. Scroll down to see that the root cause for latency has been automatically identified by X-Ray Analytics based on the trace data captured. Refer to the screenshot below for guidance.
6. With the one set of filters already applied, click on the **Compare** box at the top right of the Analytics page.

## Task 9: Create X-Ray Group

1. In the AWS Management Console on the Services menu, navigate to **X-Ray**.
2. Click on the **Groups** menu under **Configurations** and then select **Create group**.
3. Name the group **Higherlatency** and enter the following filter expression.
```
responsetime > 2
```
4. Click the **Create Group** button. Leave the default configuration for X-Ray Insights and Tags.
5. Navigate back to the **Service Map** in the left navigation menu.
6. Select **Higherlatency** from the default drop down.
7. Go to AWS XRay namespace on CloudWatch Metrics.
8. Click on **Group Metrics** in the **All metrics** tab towards the bottom of the screen.
9. Check the box next to **Higherlatency**.
10. Click on the **Graphed metrics** tab. You will see a new metric called ApproximateTraceCount created for the group Higherlatency which you just created.

## End Lab 

1. Open the **AWS Cloud9**  instance (named observability-lab) and execute the following script to remove all the resources created in this lab:
```
curl https://raw.githubusercontent.com/aws-samples/one-observability-demo/main/PetAdoptions/cdk/pet_stack/resources/destroy_stack.sh | bash
```
2. Open **AWS CloudShell**  and execute the following command
```
aws cloudformation delete-stack --stack-name C9-Observability-lab
```



